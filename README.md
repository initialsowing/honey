# Sugar:

A way to gain ownership over the health experience. Primarily a sugar tracking log with a holistic, educative approach. Empowered with data and personalized tips a user can talk confidently with their doctor and discuss tips received to take charge over their health experience.

# Screenshot:

![Sugar Screenshot](/path/to/image.jpg)

# Tech

MEN full stack

nodejs
Express
MongoDB

HTML
CSS
JS

# Demo:

(Sugar)[https://sei42-project1-sugar.herokuapp.com]

# Next Steps:

1. Graphs to communicate trends
2. Tips gleamed by analysis
3. Animations
4. Food journal
5. Food AI API for image classification in food journal
6. Personalized trigger foods
